Projet d'étudiant mené à 4 (Ivan KRIVOKUCA, Lazar ANDJELOVIC, Zicheng FANG, Aghiles SI ALI) à propos de la visualisation de donnés du classement thématique des sujets de journaux télévisés présent sur le site [ data.gouv.fr](https://www.data.gouv.fr/fr/datasets/classement-thematique-des-sujets-de-journaux-televises-janvier-2005-septembre-2020/#resources)

Toute la présentation se fait depuis le notebook **projet_vdd.ipynb**
